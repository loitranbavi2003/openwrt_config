apt-get -y update
apt-get -y upgrade
apt-get -y install nano
apt-get -y install zip

apt-get -y install build-essential ccache ecj fastjar file g++ gawk \
gettext git java-propose-classpath libelf-dev libncurses5-dev \
libncursesw5-dev libssl-dev python2.7-dev python3 unzip wget \
python3-distutils python3-setuptools python3-dev rsync subversion \
swig time xsltproc zlib1g-dev

#git clone https://git.openwrt.org/openwrt/openwrt.git
git clone https://github.com/openwrt/openwrt.git

cd openwrt

git pull
git branch -a
git checkout v23.05.0

./scripts/feeds update -a
./scripts/feeds install -a

#nano package/base-files/files/etc/banner

make menuconfig

export FORCE_UNSAFE_CONFIGURE=1
make -j24 V=99
