#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace std;

#define GPIO_RS485_LOW              0
#define GPIO_RS485_HIGH             1
#define MAX485                      "/sys/class/leds/485_REDE/brightness"

std::ofstream MAX485_brightness;

int MAX485_Enable(void);
void MAX485_SetUp(void);
void MAX485_SetDown(void);
void MAX485_DisEnable(void);

int main()
{
    MAX485_Enable();

    while(1)
    {
        cout << "valid: 1" << endl;
        MAX485_SetUp();
        sleep(1);

        cout << "valid: 0" << endl;
        MAX485_SetDown();
        sleep(1);
    }

    MAX485_DisEnable();

    return 0;
}

int MAX485_Enable(void)
{
    MAX485_brightness.open(MAX485, std::ios::trunc);

    if (!MAX485_brightness.is_open()) 
    {
        std::cout << "Failed to open the file." << std::endl;
        return 0;
    }
    return 1;
}

void MAX485_SetUp(void)
{
    MAX485_brightness << GPIO_RS485_HIGH;  
    MAX485_brightness.flush();  
}

void MAX485_SetDown(void)
{
    MAX485_brightness << GPIO_RS485_LOW;
    MAX485_brightness.flush();
}

void MAX485_DisEnable(void)
{
    MAX485_brightness << GPIO_RS485_LOW;
    MAX485_brightness.flush();
    MAX485_brightness.close();  
}

