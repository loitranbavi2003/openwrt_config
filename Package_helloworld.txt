cd openwrt
	mkdir helloworld
		nano helloworld.c

		#include <stdio.h>
		int main(void)
		{
			printf("Lab AiOT, Hello world!\n\n");
			return 0;
		}

cd openwrt 					
	#mkdir -p my_package/example/helloworld
	my_package
		example
			helloworld
			nano Makefile

----------------------------------------------------------------------------------------------------------------------------------------			
include $(TOPDIR)/rules.mk

# Name, version and release number
# The name and version of your package are used to define the variable to point to the build directory of your package: $(PKG_BUILD_DIR)
PKG_NAME:=helloworld
PKG_VERSION:=1.0
PKG_RELEASE:=1

# Source settings (i.e. where to find the source codes)
# This is a custom variable, used below, duong dan vao file helloworld.c
SOURCE_DIR:=/home/backup/openwrt/helloworld
include $(INCLUDE_DIR)/package.mk

# Package definition; instructs on how and where our package will appear in the overall configuration menu ('make menuconfig')
define Package/helloworld
  SECTION:=examples
  CATEGORY:=Examples
  TITLE:=Hello, World!
endef

# Package description; a more verbose description on what our package does
define Package/helloworld/description
  A simple "Hello, world!" -application.
endef

# Package preparation instructions; create the build directory and copy the source code. 
# The last command is necessary to ensure our preparation instructions remain compatible with the patching system.
define Build/Prepare
		mkdir -p $(PKG_BUILD_DIR)
		cp $(SOURCE_DIR)/* $(PKG_BUILD_DIR)
		$(Build/Patch)
endef

# Package build instructions; invoke the target-specific compiler to first compile the source file, and then to link the file into the final executable
define Build/Compile
		$(TARGET_CC) $(TARGET_CFLAGS) -o $(PKG_BUILD_DIR)/helloworld.o -c $(PKG_BUILD_DIR)/helloworld.c
		$(TARGET_CC) $(TARGET_LDFLAGS) -o $(PKG_BUILD_DIR)/$1 $(PKG_BUILD_DIR)/helloworld.o
endef

# Package install instructions; create a directory inside the package to hold our executable, and then copy the executable we built previously into the folder
define Package/helloworld/install
		$(INSTALL_DIR) $(1)/usr/bin
		$(INSTALL_BIN) $(PKG_BUILD_DIR)/helloworld $(1)/usr/bin
endef

# This command is always the last, it uses the definitions and variables we give above in order to get the job done
$(eval $(call BuildPackage,helloworld))
----------------------------------------------------------------------------------------------------------------------------------------

cd openwrt
	nano feeds.conf.default
	
	src-link my_package /home/backup/openwrt/my_package

cd openwrt
	./scripts/feeds update my_package
	./scripts/feeds install -a -p my_package
	
	"Installing package 'helloworld' from mypackages."
	
make package/helloworld/compile

make menuconfig
	Example
		helloworld
	Lưu và thoát. Sau đó build lại
make -j24 V=99

---------------------------------------------
opkg install /tmp/helloworld_1.0-1_<arch>.ipk
opkg remove helloworld
rm /tmp/helloworld_1.0-1_<arch>.ipk