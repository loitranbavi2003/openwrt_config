#mkdir crosscompile
cd crosscompile
cp -r ../openwrt/staging_dir .

cd staging_dir/toolchain-mipsel_24kc_gcc-8.4.0_musl/
cp -r ../target-mipsel_24kc_musl/usr/* .
cd ../..

export STAGING_DIR=/home/backup/crosscompile/staging_dir
export TOOLCHAIN_DIR=$STAGING_DIR/toolchain-mipsel_24kc_gcc-8.4.0_musl
export LDCFLAGS=$TOOLCHAIN_DIR/lib
export PATH=$TOOLCHAIN_DIR/bin:$PATH
